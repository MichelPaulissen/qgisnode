
echo "git clone http://bitbucket.org/MichelPaulissen/qgisnode /"

echo "Adding FTP group ftpaccess"
groupadd ftpaccess
echo "Adding FTP user. Password will be requested."
useradd -m userftp -g ftpaccess -s /usr/sbin/nologin
passwd userftp
chown root /home/userftp
apt-get update
apt-get upgrade
apt-get install php5 php5-sqlite libapache2-mod-php5 php5-mcrypt vsftpd git openssh-server apache2 qgis-mapserver libapache2-mod-fcgid libapache2-mod-wsgi python-psycopg2 python-webob -y
a2enmod cgid
a2enmod rewrite
a2dissite 000-default
a2ensite qgis-web-client.conf
service apache2 reload
service vsftpd restart






echo "SSH service will be restarted. Please log in again."
service ssh restart